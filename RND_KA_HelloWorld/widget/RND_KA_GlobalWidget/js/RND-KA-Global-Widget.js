define(
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['knockout'],
	//-------------------------------------------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function (ko) {
		"use strict";
		return {
			onLoad: function (widget) {
				console.log("RND KA Global widget onLoad()");
			},
			beforeAppear: function (page) {
				console.log("RND KA Global widget beforeAppear()");
			}
		};
	}
);
