define(
	//-------------------------------------------------------------------
	// DEPENDENCIES
	//-------------------------------------------------------------------
	['knockout'],
	//-------------------------------------------------------------------
	// MODULE DEFINITION
	//-------------------------------------------------------------------
	function (ko) {
		"use strict";
		return {
			helloWorldText: "Hello World",
			onLoad: function (widget) {
			},
			beforeAppear: function (page) {
			}
		};
	}
);
